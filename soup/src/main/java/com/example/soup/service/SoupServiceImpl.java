package com.example.soup.service;

import java.util.Collection;

import com.example.soup.entities.Soup;
import com.example.soup.repo.SoupRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SoupServiceImpl implements SoupService {

    @Autowired
    private SoupRepository repo;

    @Override
    public void addSoup(Soup s) {
        repo.insert(s);
        

    }

    @Override
    public Collection<Soup> getSoups() {
        
        return repo.findAll();
    }
    
    
}
package com.example.soup.service;

import java.util.Collection;

import com.example.soup.entities.Soup;

public interface SoupService {

    void addSoup(Soup s);
    Collection<Soup> getSoups();


    
}
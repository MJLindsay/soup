package com.example.soup.rest;

import java.util.Collection;

import com.example.soup.entities.Soup;
import com.example.soup.service.SoupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/soups")
public class SoupController {

    @Autowired
    private SoupService soupService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Soup> getSoups(){
        return soupService.getSoups();
    }

    // @RequestMapping(method = RequestMethod.POST)
    // public 

    // @RequestMapping(method=RequestMethod.POST)
    // public void addSoup(@RequestBody final Soup soup) {
    //     soupService.addSoup(soup);
    // }

    @RequestMapping(method=RequestMethod.POST)
    public void addSoup(@RequestBody Soup s){
        soupService.addSoup(s);

    }
    
}
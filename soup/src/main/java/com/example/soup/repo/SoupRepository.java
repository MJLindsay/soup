package com.example.soup.repo;

import com.example.soup.entities.Soup;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SoupRepository extends MongoRepository<Soup, ObjectId>{
    
}